// main js for functions

// Show & Hide Password
let SideBar = document.querySelector('#aside-main');
let PageContent = document.querySelector('#wrapper_content');
let OCbtn = document.querySelector('.sidebar-btn');

OCbtn.addEventListener('click', function () {
  if (SideBar.classList.contains('sidebar-open')) {
    // If the sidebar is open, close it
    SideBar.style.marginRight = '-245px';
    PageContent.style.marginRight = '0';
    SideBar.classList.remove('sidebar-open'); // Remove the open class
    OCbtn.innerHTML = '<i class="fa fa-arrow-left"></i>';
  } else {
    // If the sidebar is closed, open it
    SideBar.style.marginRight = '0';
    PageContent.style.marginRight = '255px'; // Adjust this based on the sidebar width
    SideBar.classList.add('sidebar-open'); // Add the open class
    OCbtn.innerHTML = '<i class="fa fa-arrow-right"></i>';
  }

});


function onlyNumber(event) {
  // Get the ASCII code of the pressed key.
  const keyCode = event.which ? event.which : event.keyCode;

  // Allow backspace, delete, and arrow keys.
  if (keyCode === 8 || keyCode === 46 || (keyCode >= 37 && keyCode <= 40)) {
    return true;
  }

  // Only allow numeric keys.
  if (keyCode < 48 || keyCode > 57) {
    event.preventDefault();
    return false;
  }

  // Allow numeric keys.
  return true;
}

// Show and Hide password
function ShowHidePassword(inputId) {
  var input = document.getElementById(inputId);

  if (input.type === "password") {
      input.type = "text";
      this.innerHTML = '<i class="fa fa-eye-dropper"></i>';
  } else {
      input.type = "password";
  }
}





