// var

let FullName = document.querySelector("#fullname");
let NationID = document.querySelector("#nationID");
let motherName = document.querySelector("#motherName");
let mobile = document.querySelector("#mobile");
let stat = document.querySelector("#status");

let PenaltyList = document.querySelector("#penalty_list");
// table
const NewTable = document.querySelector("#New-Data-table");
const OldTable = document.querySelector("#Old-Data-Table");

// form
let TextSearch = document.querySelector("#text_search");
let StatusSearch = document.querySelector("#status_search");

// buttons
var loader = document.querySelector(".loader");
var SearchBTN = document.querySelector("#search_btn");
// ==============================================================

// get New Data Students List
function NewStudentsList(search = "", status = -1) {
  loader.style.display = "flex";
  NewTable.innerHTML = "";
  $.ajax({
    method: "GET",
    url: "NewStudentsList",
    data: {
      s_text: search,
      s_status: status,
    },
    success: function (response) {
      setTimeout(() => {
        loader.style.display = "none"; // to hide loading

        $.each(response.students.students, function (key, stu) {
          $("#New-Data-table").append(`
                    <tr>
                         <td>${stu["id"]}</td>
                        <td>${stu["NationalID"]}</td>
                        <td>${stu["fullname"]}</td>
                        <td>${stu["mobile"] != "" ? stu["mobile"] : "-"}</td>
                        <td>${getStatus(stu["status"])}</td>

                        <td class="d-flex justify-content-around p-2">
                            <div class="mybutton btn-warning" title="تفاصيل" id="info_student" data-id="${
                              stu["id"]
                            }" onclick="info_student(this)"><span class="fa fa-info-circle"></span></div>
                            </td>
                    </tr>
                `);
        });
      }, 200);
    },
    error: function (response) {
      console.log(response);
    },
  });
}

// get Old Data Students List
function OldStudentsList(search = "") {
  loader.style.display = "flex";
  OldTable.innerHTML = "";
  $.ajax({
    method: "GET",
    url: "OldStudentsList",
    data: {
      s_text: search,
    },
    success: function (response) {
      setTimeout(() => {
        loader.style.display = "none"; // to hide loading

        $.each(response.students, function (key, stu) {
          $("#Old-Data-Table").append(`
                    <tr>
                         <td>${stu["id"]}</td>
                        <td>${stu["fullname"]}</td>
                        <td>${stu["building_id"] != "" ? stu["building_id"] : "-"}</td>
                        <td>${stu["room_id"] != "" ? stu["room_id"] : "-"}</td>
                        <td>${stu['faculty'] != "" ?  stu['faculty'] : "-"}</td>

                      <td>
                        <div class="mybutton btn-primary" title="بريء الذمة" id="confirm_penalty_old" 
                 data-studentid="${stu["id"]}" 
                onclick="confirm_penalty_old(this)">
                <span class="fa fa-coins"></span>
              </div>
                      </td>
                    </tr>
                `);
        });
      }, 200);
    },
    error: function (response) {
      console.log(response);
    },
  });
}

function info_student(button) {
  let studentID = $(button).data("id");
  $.ajax({
    method: "GET",
    url: "get_student_data",
    data: {
      id: studentID,
    },
    success: function (response) {
      console.log(response);
      FullName.innerHTML = response.student["fullname"];
      NationID.innerHTML = response.student["NationalID"];
      motherName.innerHTML =
        response.student["mother"] != "" ? response.student["mother"] : "-";
      mobile.innerHTML =
        response.student["mobile"] != "" ? response.student["mobile"] : "-";
      stat.innerHTML = getStatus(response.student["status"]);
      PenaltyList.innerHTML = "";
      count = 1;
      $.each(response.penalty, function (key, pen) {
        const rowId = `penalty-row-${key}`; // Unique ID for each row

        $("#penalty_list").append(`
          <tr id="${rowId}" class="small">
            <td>${count++}</td>
            <td>${pen["penaltyid"]}</td>
            <td>${pen["value"]} ل.س</td>
            <td>${getPenaltyType(pen["ptype"])}</td>
            <td id="unit-name-${key}">Loading...</td> <!-- Placeholder -->
            <td>${pen["roomno"]}</td>
            <td>كلية ${pen["faculty"]}</td>
           <td id="status-${pen['penaltyid']}">${getStatus(pen["status"])}</td>

            <td class="d-flex justify-content-around p-2">
               <a target="_blank" title="طباعة إيصال" href="cut_penalty/${pen["penaltyid"]}/${pen["stdid"]}/${2}" 
                class="mybutton mx-1 btn-primary" id="cut_penalty">
                <span class="fa fa-file-invoice-dollar"></span>
              </a>
            ${(pen['status'] == 0) ? '' : `<div class="mybutton btn-warning" title="تسديد الغرامة" id="confirm_penalty" 
                data-penaltyid="${
                  pen["penaltyid"]
                }" data-studentid="${pen["stdid"]}" 
                onclick="confirm_penalty(this)">
                <span class="fa fa-coins"></span>
              </div>`}        
            </td>
          </tr>
        `);

        // Fetch unit name asynchronously and update the corresponding cell
        getUnitName(pen["housebuilding_id"], function (name) {
          $(`#unit-name-${key}`).text(name); // Update the cell with the fetched name
        });
      });

      $("#student_info").modal("show");
    },
    error: function (response) {
      console.log(response);
    },
  });
}

function confirm_penalty(button) {
  let pID = $(button).data("penaltyid");
  let stID = $(button).data("studentid");
  $.ajax({
    method: "get",
    url: "confirm_penalty",
    data: {
      pen_id: pID,
      st_id: stID,
      status: 0,
    },
    success: function (response) {
      if (response.status) {
        alertify.set("notifier", "position", "top-right");
        alertify.success(response.massage);
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(response.massage);
      }
          // Update the status in the UI          
          $(`#status-${pID}`).html(getStatus(0));
    },
  });
}


function confirm_penalty_old(button) {
  let stID = $(button).data("studentid");
  $.ajax({
    method: "get",
    url: "confirm_penalty_old",
    data: {
      st_id: stID,
      status: 0,
    },
    success: function (response) {
      if (response.status) {
        alertify.set("notifier", "position", "top-right");
        alertify.success(response.massage);
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(response.massage);
      }
      OldStudentsList();
    },
  });
}

// the Page is Ready
$(document).ready(function () {
  // Penalty list
  NewStudentsList();
  OldStudentsList();

  SearchBTN.addEventListener("click", function () {
    text = TextSearch.value;
    stat = StatusSearch.value;
    NewStudentsList(text, stat);
    OldStudentsList(text)
  });
});
