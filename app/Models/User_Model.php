<?php

namespace App\Models;

use CodeIgniter\Model;

class User_Model extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'name',
        'username',
        'password',
        'per_addpenalty',
        'per_cutpayment',
        'per_confpayment',
        'per_users',
        'per_import',
        'active'
    ];

    protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = false;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];




    // functions

    public function getUsersList($page = 1)
    {
        $perPage = 10;

        $Users = $this->paginate($perPage, 'default', $page);
        
        // Return both Users and the pager object
        return [
            'users' => $Users,
            'pager' => $this->pager
        ];   
     }
     public function getUser($username)
     {
         return $this->where('username', $username)->first();
     }

     public function getuserpass($usernam){
        $query = $this->db->query('select password from users WHERE username = ?', [$usernam]);
        return $query->getRow()->password;

     }

    public function update_password($usernam,$pass){
        $query = $this->db->query('update users set password = ? WHERE username = ?', [$pass,$usernam]); 
        if ($query) {return true;} else {return false;}
       
    }
  //  test

}