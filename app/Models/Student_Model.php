<?php

namespace App\Models;

use CodeIgniter\Model;

use function PHPUnit\Framework\isNull;

class Student_Model extends Model
{
    protected $table      = 'students';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'nationID',
        'fullname',
        'mother',
        'mobile',
        'address',
        'notes',
        'unit',
        'room',
        'university',
        'study_year',
        'study_season',
        'status',
        'NationalID'
    ];

    /* protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = false;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];*/




    // functions
    public function getNewStudentsList($search = '', $status = -1)
    {
//test
        if ($search != '') {
            if ($status == -1) {
                $students = $this->like('fullname', $search)
                    ->orLike('NationalID', $search)
                    ->orLike('mobile', $search)
                    ->orderBy('id', 'asc')
                    ->findAll(50);
            } else {
                $students = $this->like('fullname', $search)
                ->orLike('NationalID', $search)
                ->orLike('mobile', $search)
                ->where('status', $status)
                ->orderBy('id', 'asc')
                ->findAll(50);
            }
        } else {
            if ($status == -1) {
                $students = $this->orderBy('id', 'asc')->findAll(50);
            } else {
                $students = $this->where('status', $status)
                ->orderBy('id', 'asc')
                ->findAll(50);
            }
        }
        return ['students' => $students,];
    }


    public function getStudentsData($id)
    {
        $student = $this->find($id);
        return $student;
    }

    public function studentExist($nation)
    {

        return $this->where('NationalID', $nation)->findAll();
    }

    public function getroomstudents($rom, $unt)
    {
        //$sql = "select students.id,students.fullname from students , buildingdetails WHERE students.id = buildingdetails.studentid and buildingdetails.roomno = '".$rom."' and buildingdetails.housebuilding_id = '".$unt."';";
        $query = $this->db->query('select students.id,students.fullname from students , buildingdetails WHERE students.id = buildingdetails.studentid and buildingdetails.roomno = ? and buildingdetails.housebuilding_id = ?', [$rom, $unt]);
        return $query->getResultArray();
    }


    public function getpenaltysingle_detail($pid)
    {
        //$sql = "select students.id,students.fullname from students , buildingdetails WHERE students.id = buildingdetails.studentid and buildingdetails.roomno = '".$rom."' and buildingdetails.housebuilding_id = '".$unt."';";
        $query = $this->db->query('select * from  penaltysingals where id  = ? ', [$pid]);
        return $query->getResultArray();
    }

    public function getpenaltyCollective_detail($pid)
    {
        //$sql = "select students.id,students.fullname from students , buildingdetails WHERE students.id = buildingdetails.studentid and buildingdetails.roomno = '".$rom."' and buildingdetails.housebuilding_id = '".$unt."';";
        $query = $this->db->query('select * from  penaltycollectives where id  = ?', [$pid]);
        return $query->getResultArray();
    }



    public function getpenaltysingle_student_detail($pid)
    {
        //$sql = "select students.id,students.fullname from students , buildingdetails WHERE students.id = buildingdetails.studentid and buildingdetails.roomno = '".$rom."' and buildingdetails.housebuilding_id = '".$unt."';";
        $query = $this->db->query('select * from studentpenalty, students, buildingdetails where students.id = studentpenalty.stdid AND students.id = buildingdetails.studentid AND studentpenalty.penaltyid = ?', [$pid]);
        return $query->getResultArray();
    }

    public function GetPenaltyByStudentId($sid)
    {
        $query = $this->db->query('select * from studentpenalty, students, buildingdetails,penaltysingals where studentpenalty.stdid = students.id AND studentpenalty.penaltyid = penaltysingals.id AND students.id = buildingdetails.studentid AND students.id = ?', [$sid]);
        return $query->getResultArray();
    }

    //search type : id = national id, mob = mobile , fullname = full name
    // $stat : -1 = all, 0 : no penalty, 1 : there is penalty , 2 : paiding progress
    public function getstudent_detail($searchword, $stat)
    {

        if ($stat = -1) {
            $query = $this->db->query("SELECT * FROM `students` , `buildingdetails` where `students`.`id` = buildingdetails.studentid  and 
                 (students.fullname LIKE '%" . $searchword . "%' or students.mobile LIKE '%" . $searchword . "%' or students.NationalID like '%" . $searchword . "%');");
        } elseif ($stat = 0) {
            $query = $this->db->query("SELECT * FROM `students` , `buildingdetails` where `students`.`id` = buildingdetails.studentid and students.status = '$stat' and
                 (students.fullname LIKE '%" . $searchword . "%' or students.mobile LIKE '%" . $searchword . "%' or students.NationalID like '%" . $searchword . "%');");
        } elseif ($stat = 1) {
            $query = $this->db->query("SELECT * FROM `students` , `buildingdetails` where `students`.`id` = buildingdetails.studentid and students.status = '$stat' and 
                 (students.fullname LIKE '%" . $searchword . "%' or students.mobile LIKE '%" . $searchword . "%' or students.NationalID like '%" . $searchword . "%');");
        }
        if ($stat = 2) {
            $query = $this->db->query("SELECT * FROM `students` , `buildingdetails` where `students`.`id` = buildingdetails.studentid and students.status = '$stat' and
                 (students.fullname LIKE '%" . $searchword . "%' or students.mobile LIKE '%" . $searchword . "%' or students.NationalID like '%" . $searchword . "%');");
        }


        return $query->getResultArray();
    }
    public function getstudent_details_old($searchword, $statusval)
    {
        if ($statusval = -1) {
            $query = $this->db->query("
            SELECT * FROM `oldstudents0`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents1`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents2`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents3`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents4`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents5`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents6`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents7`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents8`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents9`  where   fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents10` where   fullname LIKE '%" . $searchword . "%';
            ");
        } else {
            $query = $this->db->query("
            SELECT * FROM `oldstudents0`  where  oldstudents0.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents1`  where  oldstudents1.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents2`  where  oldstudents2.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents3`  where  oldstudents3.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents4`  where  oldstudents4.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents5`  where  oldstudents5.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents6`  where  oldstudents6.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents7`  where  oldstudents7.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents8`  where  oldstudents8.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents9`  where  oldstudents9.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents10` where  oldstudents10.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            ");
        }

        return $query->getResultArray();
    }

    public function getoldstudent($searchword)
    {
        if ($searchword == '') {
            $query = $this->db->query("SELECT * FROM `oldstudents10` limit 0,50;");
            return $query->getResultArray();
        }
      else {
        $query0 = $this->db->query("SELECT * FROM `oldstudents0`  where  fullname LIKE '%" . $searchword . "%';");
        $result0 =  $query0->getResultArray();
        $query1 = $this->db->query("SELECT * FROM `oldstudents1`  where  fullname LIKE '%" . $searchword . "%';");
        $result1 =  $query1->getResultArray();
        $query2 = $this->db->query("SELECT * FROM `oldstudents2`  where  fullname LIKE '%" . $searchword . "%';");
        $result2 =  $query2->getResultArray();
        $query3 = $this->db->query("SELECT * FROM `oldstudents3`  where  fullname LIKE '%" . $searchword . "%';");
        $result3 =  $query3->getResultArray();
        $query4 = $this->db->query("SELECT * FROM `oldstudents4`  where  fullname LIKE '%" . $searchword . "%';");
        $result4 =  $query4->getResultArray();
        $query5 = $this->db->query("SELECT * FROM `oldstudents5`  where  fullname LIKE '%" . $searchword . "%';");
        $result5 =  $query5->getResultArray();
        $query6 = $this->db->query("SELECT * FROM `oldstudents6`  where  fullname LIKE '%" . $searchword . "%';");
        $result6 =  $query6->getResultArray();
        $query7 = $this->db->query("SELECT * FROM `oldstudents7`  where  fullname LIKE '%" . $searchword . "%';");
        $result7 =  $query7->getResultArray();
        $query8 = $this->db->query("SELECT * FROM `oldstudents8`  where  fullname LIKE '%" . $searchword . "%';");
        $result8 =  $query8->getResultArray();
        $query9 = $this->db->query("SELECT * FROM `oldstudents9`  where  fullname LIKE '%" . $searchword . "%';");
        $result9 =  $query9->getResultArray();
        $query10 = $this->db->query("SELECT * FROM `oldstudents10`  where  fullname LIKE '%" . $searchword . "%';");
        $result10 =  $query10->getResultArray();
        $mergedResults = array_merge($result0, $result1, $result2,$result3,$result4,$result5,$result6,$result7,$result8,$result9,$result10);
        return $mergedResults;
       
            /*$query = $this->db->query("
            SELECT * FROM `oldstudents0`  where  oldstudents0.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents1`  where  oldstudents1.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents2`  where  oldstudents2.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents3`  where  oldstudents3.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents4`  where  oldstudents4.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents5`  where  oldstudents5.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents6`  where  oldstudents6.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents7`  where  oldstudents7.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents8`  where  oldstudents8.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents9`  where  oldstudents9.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            SELECT * FROM `oldstudents10` where  oldstudents10.status = '" . $statusval . "' and fullname LIKE '%" . $searchword . "%';
            ");*/
        }

        //$result0 =  $query->getResultArray();
       // $mergedResults = array_merge($result1, $result2);
    }
    public function ChangeStatusByPenaltyId($pen, $stdid, $statusval)
    {
        $query = $this->db->query("UPDATE `studentpenalty` set `status`= " . $statusval . " WHERE stdid = " . $stdid . " and penaltyid = " . $pen . "");
        if ($query) return true;
        else return false;
    }
}
