<?php

namespace App\Controllers;

use App\Models\User_Model;

class Setting extends BaseController
{
    public function __construct()
    {
        $this->user = new User_Model();
        $this->unit = new Housing_Unit_model();
        $this->suite = new Suits_Model();
        $this->session = session();
    }


    public function users_list()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }

        $data['users'] = $this->user->getUsersList();

        $data['title'] = "المستخدمين والسماحيات";
        return view('settings/users_list', $data);
    }

    public function getUsers()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }

        $page = $this->request->getVar('page');
        $data['users'] = $this->user->getUsersList($page);
        return $this->response->setJSON($data);
    }

    public function add_edit_user()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        if ($this->request->isAJAX() && $this->request->getMethod() == 'post') {

            $user_id = $this->request->getPost('user_id');
            $info['name'] = $this->request->getPost('user_fullname');
            $info['username'] = $this->request->getPost('user_name');
            $info['password'] = password_hash($this->request->getPost('user_password'), PASSWORD_DEFAULT);
            $info['per_addpenalty'] = ($this->request->getPost('add_penalty') == 1) ? 1 : 0;
            $info['per_cutpayment'] = ($this->request->getPost('cut_payment') ==  1) ? 1 : 0;
            $info['per_confpayment'] = ($this->request->getPost('confirm_payment') == 1) ? 1 : 0;
            $info['per_users'] = ($this->request->getPost('users_permessions') == 1) ? 1 : 0;
            $info['per_import'] = ($this->request->getPost('emport_from_excel') == 1) ? 1 : 0;
            $info['active'] = $this->request->getPost('user_active');

            if ($user_id == '') {
                // add new user
                if ($this->user->save($info)) {
                    $data['status'] = "success";
                    $data['massage'] = "تم إضافة مستخدم جديد بنجاح !";
                } else {
                    $data['status'] = "error";
                    $data['massage'] = "تعذر إضافة مستخدم بسبب أخطاء في الداتا بيس !!!";
                }

            } else {

                // add new user
                if ($this->user->update($user_id, $info)) {
                    $data['status'] = "success";
                    $data['massage'] = "تم تعديل بيانات مستخدم بنجاح !";
                } else {
                    $data['status'] = "error";
                    $data['massage'] = "تعذر تعديل بيانات مستخدم بسبب أخطاء في الداتا بيس !!!";
                }

            }
            return $this->response->setJSON($data);
        }
    }

    public function get_user_data_by_id()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $user_id = $this->request->getVar('id');
        $data['user'] = $this->user->find($user_id);
        return $this->response->setJSON($data);
    }

    public function housing_units()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $data['units'] = $this->unit->getUnitsList();
        $data['title'] = "الوحدات السكنية";
        return view('settings/housing_units', $data);
    }

    public function getUnits()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $page = $this->request->getVar('page');
        $data['units'] = $this->unit->getUnitsList($page);
        return $this->response->setJSON($data);
    }

    public function get_unit_data_by_id()
    {
        $unit_id = $this->request->getVar('id');
        $data['unit'] = $this->unit->find($unit_id);
        return $this->response->setJSON($data);
    }

    public function add_edit_unit()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        if ($this->request->isAJAX() && $this->request->getMethod() == 'post') {

            $unit_id = $this->request->getPost('unit_id');
            $info['name'] = $this->request->getPost('unit_name');
            $info['supervisor'] = $this->request->getPost('supervisor_name');
            $info['type'] = $this->request->getPost('unit_type');
            if ($unit_id == '') {
                // add new user
                if ($this->unit->save($info)) {
                    $data['status'] = "success";
                    $data['massage'] = "تم إضافة وحدة سكنية جديدة بنجاح !";
                } else {
                    $data['status'] = "error";
                    $data['massage'] = "تعذر إضافة وحدة سكنية بسبب أخطاء في الداتا بيس !!!";
                }

            } else {

                // add new user
                if ($this->unit->update($unit_id, $info)) {
                    $data['status'] = "success";
                    $data['massage'] = "تم تعديل بيانات الوحدة السكنية بنجاح !";
                } else {
                    $data['status'] = "error";
                    $data['massage'] = "تعذر تعديل بيانات الوحدة السكنية بسبب أخطاء في الداتا بيس !!!";
                }

            }
            return $this->response->setJSON($data);
        }
    }

    public function get_unit_name_by_id()
    {
        $id = $this->request->getVar('id');
        $name = $this->unit->select('name')->find($id);
        return $this->response->setJSON($name);
    }


    public function suites()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $data['suites'] = $this->suite->getSuitesList();
        $data['units'] = $this->unit->findAll();
        $data['title'] = "الأجنحة";
        return view('settings/suites_list', $data);
    }

    public function getSuite()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $page = $this->request->getVar('page');
        $data['suites'] = $this->suite->getSuitesList($page);
        return $this->response->setJSON($data);
    }
    public function get_suite_data_by_id()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $suite_id = $this->request->getVar('id');
        $data['suite'] = $this->suite->find($suite_id);
        return $this->response->setJSON($data);
    }

    public function add_edit_suite()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        if ($this->request->isAJAX() && $this->request->getMethod() == 'post') {

            $suite_id = $this->request->getPost('suite_id');
            $info['name'] = $this->request->getPost('suite_name');
            $info['firstroom'] = $this->request->getPost('suite_first_room');
            $info['lastroom'] = $this->request->getPost('suite_last_room');
            $info['housebuilding_id'] = $this->request->getPost('unit_id');
            if ($suite_id == '') {
                // add new user
                if ($this->suite->save($info)) {
                    $data['status'] = "success";
                    $data['massage'] = "تم إضافة جناح جديدة بنجاح !";
                } else {
                    $data['status'] = "error";
                    $data['massage'] = "تعذر إضافة جناح بسبب أخطاء في الداتا بيس !!!";
                }

            } else {

                // add new user
                if ($this->suite->update($suite_id, $info)) {
                    $data['status'] = "success";
                    $data['massage'] = "تم تعديل بيانات الجناح بنجاح !";
                } else {
                    $data['status'] = "error";
                    $data['massage'] = "تعذر تعديل بيانات الجناح بسبب أخطاء في الداتا بيس !!!";
                }

            }
            return $this->response->setJSON($data);
        }
    }


    public function change_password()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        
        $username = session()->get('username');
        $currentpass = $this->request->getPost('currentpass');
        $newpass = $this->request->getPost('newpassword');
        
        $userpass = $this->user->getuserpass($username);
        
        if ($newpass && password_verify($currentpass, $userpass)) {
            $hashedNewPass = password_hash($newpass, PASSWORD_DEFAULT);
            $updated = $this->user->update_password($username, $hashedNewPass);
        
            if ($updated) {
                // Optionally, set a success message
                session()->setFlashdata('success', 'تم تغيير كلمة السر');
            } else {
                // Optionally, set an error message
                session()->setFlashdata('error', 'لم يتم تغيير كلمة السر');
            }
        
            return redirect()->to(base_url('change-password'));
        } else {
            $data['validation'] = service('validation');
            $data['validation']->setError('currentpass', 'كلمة السر الحالية غير صحيحة');
        }
        
        $data['title'] = "تغيير كلمة السر";
        
        return view('settings/change_password', $data);
        
        
    }
}
