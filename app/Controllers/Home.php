<?php

namespace App\Controllers;

use App\Models\student_Model;
use App\Models\student_penalty_Model;
use App\Models\User_Model;


class Home extends BaseController
{
    public function __construct()
    {
        $this->usermodel = new User_Model();
        $this->session = session();
    }

    public function index()
    {
        return redirect()->to(base_url('login'));
    }

   
    // Dashboard
    public function dashboard()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }


        $data['title'] = "لوحة التحكم";
        return view('dashboard', $data);
    }

   
}
