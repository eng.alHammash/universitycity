<?php

namespace App\Controllers;

use App\Models\User_Model;


class Auth extends BaseController
{
    public function __construct()
    {
        $this->new_student = new student_Model();
        $this->usermodel = new User_Model();
        $this->student_penalty = new student_penalty_Model();
        $this->session = session();
    }

   
    // Login 
    public function login()
    {
        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'username' => 'required',
                'password' => 'required'
            ];

            if (! $this->validate($rules)) {
                $data['validation'] = $this->validator;
            } else {

                $user = $this->usermodel->getUser($this->request->getVar('username'));

                if ($user && password_verify($this->request->getVar('password'), $user['password'])) {
                    $sessionData = [
                        'id'       => $user['id'],
                        'username' => $user['username'],
                        'isLoggedIn' => true
                    ];

                    session()->set($sessionData);
                    return redirect()->to(base_url('dashboard'));
                } else {
                    $data['validation'] = service('validation')->setError('username', 'خطأ اسم المستخدم أو كلمة السر');
                }
            }
        }

        $data['title'] = "تسجيل الدخول";
        return view('auth/login', $data);
    }
    //=================================

    public function logout()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        session()->destroy();
        return redirect()->to('/login');
    }

    //=================================
    public function pass()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        echo password_hash("admin", PASSWORD_DEFAULT);
    }
    //=================================

  
}
