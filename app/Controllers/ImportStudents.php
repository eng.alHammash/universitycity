<?php

namespace App\Controllers;

use CodeIgniter\HTTP\ResponseInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use App\Models\Student_Model;
use App\Models\Buildingdetails_Model;
use App\Models\Housing_Unit_model;

class ImportStudents extends BaseController
{

    public function __construct()
    {
        $this->students = new Student_Model();
        $this->buildingdetails = new Buildingdetails_Model();
        $this->unit = new Housing_Unit_model();
        $this->session = session();
    }
    public function index()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        
        $data['units'] = $this->unit->findAll();
        $data['title'] = "Import Excel";
        return view('importxls/import_students' , $data);
    }


    public function upload()
    {
        $file =  $this->request->getFile('excel_file');
        $unit =  $this->request->getPost('unit');
        $year =  $this->request->getPost('year');
        $season =  $this->request->getPost('season');
          
        if (!$file->isValid()) {
            return redirect()->back()->with('error', 'No file uploaded.');
        }

        $spreadsheet = new Spreadsheet();
        $reader = new Xlsx();
        $spreadsheet = $reader->load($file->getTempName());
        $worksheet = $spreadsheet->getActiveSheet();

        // Process the data from the worksheet
        $data = [];
        foreach ($worksheet->getRowIterator(1) as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $rowData = [];

            foreach ($cellIterator as $cell) {

                $rowData[] = $cell->getValue();
                
            }
            $data[] = $rowData;
        
          
        }
        foreach($data as $da){
            if (trim($da[1]) != 'اسم الطالب'){
                $studentdata['fullname'] = $da[1];
                $studentdata['mother'] =  '';
                $studentdata['mobile'] = '';
                $studentdata['status'] = '0';
                $studentdata['NationalID'] = $da[0];
                $res =$this->students->studentExist($studentdata['NationalID']) ;
              
                if (count($res)>0){
                    foreach($res as $row){
                        $inserted_id = $row['id']; 
                    }
                }else{
                    $this->students->insert($studentdata);
                    $inserted_id = $this->students->getInsertID();
                }
               
                $room = explode("/", $da[2]);
                if ($room[1] == 'فرز تلقائي'){
                    $roomnumber = 0;
                }
                else{
                    $roomnumber =  $room[1];
                }
                $buildingdata['roomno'] =$roomnumber;
                $buildingdata['importdate'] = date('Y-m-d');
                $buildingdata['faculty'] = $da[3];
                $buildingdata['class'] =$da[4];
                $buildingdata['studentid'] =  $inserted_id;
                
                $buildingdata['study_year'] = $year;
                $buildingdata['study_part'] = $season;
                $buildingdata['housebuilding_id'] = $unit;
                $this->buildingdetails->insert($buildingdata);
                 
            }
                    

                    


      
        }
        
        // Save the data to the database or perform other actions
        // ...

        //return redirect()->back()->with('success', 'Excel file imported successfully.');
      
    }
}