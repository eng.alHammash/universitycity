<?php

// firstly to load this helper you need add name in Helpers in BaseControllers; 

function IsLogin()
{
    $session = session();

    $login = $session->get('login');
    if($login)
    {
        return true;
    }else{
        return false;
    }
}

function GetNameUnitById($id){

    $userModel = model(Housing_Unit_model::class);
    return $userModel->GetNameUnitById($id);
}