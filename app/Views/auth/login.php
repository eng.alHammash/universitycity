<?= $this->extend('layouts/master'); ?>

<?= $this->section('content'); ?>

<div class="login-page">

    <div class="login-form">
        <?= form_open('login') ?>
        <?= csrf_field() ?>
        <div class="login">
            <div class="login-img">
                <img src="<?= base_url('public/assets/images/logo.jpg') ?>" alt="">
            </div>
            <hr>
            <div class="login-text">تسجيل الدخول</div>
            <?php if (isset($validation)) : ?>
                <div class="alert alert-danger"><?= $validation->getError('username') ?></div>
            <?php endif; ?>
            <div class="group">
                <label for="username">اسم المستخدم :</label>
                <input type="text" name="username" id="username" class="form-control" placeholder="ادخل اسم المستخدم ...">
            </div>
            <div class="group">
                <label for="password">كلمة السر :</label>
                <div class=" input-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="ادخل كلمة السر ...">
                    <div class="input-group-text cursor-pointer" onclick="ShowHidePassword('password')">
                        <i class="fa fa-eye"></i>
                    </div>
                </div>
            </div>
            <hr>
            <div class="login-footer">
                <input type="submit" value="تسجيل الدخول" class="mybutton mybutton-primary">
            </div>
        </div>
        <?= form_close(); ?>
    </div>

</div>

<?= $this->endSection(); ?>