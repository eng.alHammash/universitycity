<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>


<?php if (session('error')) : ?>
    <div class="alert alert-danger"><?= session('error') ?></div>
<?php endif; ?>

<?php if (session('success')) : ?>
    <div class="alert alert-success"><?= session('success') ?></div>
<?php endif; ?>

<div class="box w-50 mx-auto">
    <form action="<?= base_url('import-student/upload') ?>" method="post" enctype="multipart/form-data">
        <div class="box_header">
            <div class="box_title">استيراد الطلاب</div>
        </div>
        <div class="box_body">

            <div class="group">
                <label for="unit">الوحدة :</label>
                <select name="unit" id="unit" class="form-control">
                    <?php
                    foreach ($units as $un) {
                    ?>

                        <option value="<?= $un['id'] ?>"><?= $un['name'] ?></option>
                    <?php
                    }

                    ?>
                </select>
            </div>
            <div class="group">
                <label for="year">السنة الدراسية :</label>
                <select name="year" id="year" class="form-control">

                </select>
            </div>
            <div class="group">
                <label for="season">الفصل الدراسي :</label>
                <select name="season" id="season" class="form-control">
                    <option value="0">الأول</option>
                    <option value="1">الثاني</option>
                    <option value="2">صيفي</option>
                </select>
            </div>
            <div class="group">
                <label for="excel_file">اختر ملف الإكسل :</label>
                <input type="file" name="excel_file" id="excel_file" class="form-control" required>
            </div>

        </div>
        <div class="box_footer">
            <button class="mybutton btn-warning" type="submit">إستيراد</button>
        </div>
    </form>
</div>

<script>
    const select = document.getElementById('year');
    for (let year = 2024; year <= 2090; year++) {
        const option = document.createElement('option');
        option.value = year;
        option.textContent = year;
        select.appendChild(option);
    }
</script>

<?= $this->endSection(); ?>