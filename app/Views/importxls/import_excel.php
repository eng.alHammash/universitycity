<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>

    <h1>Import Excel</h1>

    <?php if (session('error')) : ?>
        <div class="alert alert-danger"><?= session('error') ?></div>
    <?php endif; ?>

    <?php if (session('success')) : ?>
        <div class="alert alert-success"><?= session('success') ?></div>
    <?php endif; ?>

    <form action="<?= base_url('import-excel/upload') ?>" method="post" enctype="multipart/form-data">
        <div>
            <label for="excel_file">Select Excel File:</label>
            <input type="file" name="excel_file" id="excel_file" required>
        </div>
        <button type="submit">Import</button>
    </form>
</body>
</html>


<?= $this->endSection(); ?>