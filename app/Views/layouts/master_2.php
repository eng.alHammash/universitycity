<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $title;  ?></title>
    <meta name="viewport" content="width=device-width, maximum-scale=5, initial-scale=1">

    <!-- coston fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+Arabic:wght@100..900&display=swap" rel="stylesheet">


    <!-- icon website -->
    <link rel="shortcut icon" href="<?= base_url('public/assets/images/logo.jpg') ?>" type="image/x-icon">

    <!--  Core v5.0.2  -->
    <?= link_tag('public/core/css/core.min.css'); ?>

    <!--  Vendors v5.0.0 -->
    <?= link_tag('public/core/css/vendor_bundle.min.css'); ?>

    <!-- alertifyjs 1.13.1  RTL -->
    <?= link_tag('public/assets/css/alertify.rtl.min.css'); ?>

    <!-- Font Awesome Free 6.1.1 -->
    <?= link_tag('public/assets/css/all.css'); ?>

    <!-- Custom Css style -->
    <?= link_tag('public/assets/css/mystyle.css'); ?>

    <!-- jquery Version 3.6.1 -->
    <?= script_tag('public/assets/js/jquery.min.js'); ?>


</head>

<body class="layout-admin aside-sticky bg-lime header-sticky" data-s2t-class="btn-primary btn-sm bg-gradient-default rounded-circle border-0">

    <div id="wrapper" class="d-flex align-items-stretch flex-column">

        <!--  header -->

        <?= $this->include("include/header_2");   ?>

        <!-- /Header -->

        <!-- sidebar -->

        <?= $this->include("include/sidebar");   ?>

        <!-- /sidebar -->

        <!-- content -->
        <div id="wrapper_content" class="d-flex flex-fill">

            <main  class="flex-fill mx-auto page">

                <?= $this->renderSection("content"); ?>

            </main>
            
        </div>
        <!-- /content -->

    </div>
   
        <!-- footer -->

        <?= $this->include("include/footer_2");   ?>

        <!-- /footer -->

    <!-- Custom Js Functions  -->
    <?= script_tag('public/assets/js/main.js'); ?>

    <!--  Core v5.0.0  -->
    <?= script_tag('public/core/js/core.min.js'); ?>

    <!-- Vendors v5.0.0 -->
    <?= script_tag('public/core/js/vendor_bundle.min.js'); ?>

    <!-- alertifyjs - v1.13.1 -->
    <?= script_tag('public/assets/js/alertify.min.js'); ?>

    <!-- Font Awesome Free 6.1.1 -->
    <?= script_tag('public/assets/js/all.js'); ?>

</body>

</html>