<!-- this layout to Login Pages -->
<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=chrome">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- coston fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+Arabic:wght@100..900&display=swap" rel="stylesheet">

    <!-- Font Awesome Free 6.1.1  -->
    <?= link_tag(base_url('public/assets/css/all.css'), 'stylesheet', 'text/css');  ?>

    <!-- RTL Bootstrap v5.1.3  -->
    <?= link_tag(base_url('bootstrap/css/bootstrap.rtl.min.css'), 'stylesheet', 'text/css'); ?>

    <!-- RTL alertifyjs 1.13.1 -->
    <?= link_tag(base_url('public/assets/css/alertify.rtl.min.css'), 'stylesheet', 'text/css');  ?>

    <!--  Default alertifyjs 1.13.1 -->
    <?= link_tag(base_url('public/assets/css/default.min.css'), 'stylesheet', 'text/css'); ?>

    <!-- Custom Css Style For Login Pages -->
    <?= link_tag(base_url('public/assets/css/login.css'), 'stylesheet', 'text/css'); ?>

    <!-- logo -->
    <link rel="shortcut icon" href="<?= base_url('public/assets/images/logo.jpg') ?>" type="image/x-icon">

    <!-- jQuery v3.6.1  -->
    <?= script_tag(base_url('public/assets/js/jquery.min.js')); ?>


    <title>
        <?= $title ?>
    </title>
</head>

<body>


    <!-- header -->
    <?= $this->include('include/header.php') ?>

    <div class="page">
        <!-- content -->
        <?= $this->renderSection("content") ?>
    </div>

    <!-- footer -->
    <?= $this->include('include/footer.php') ?>


</body>

<!-- Custom Js functions -->
<?= script_tag(base_url('public/assets/js/main.js')); ?>

<!-- Js to font awesome -->
<?= script_tag(base_url('public/assets/js/all.js')); ?>

<!-- full JS bootstrap 5.1.3 -->
<?= script_tag(base_url('bootstrap/js/bootstrap.bundle.min.js')); ?>

<!-- Js to alertifyJS -->
<?= script_tag(base_url('public/assets/js/alertify.min.js')); ?>

</html>